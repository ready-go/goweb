package main

import (
	"github.com/gin-gonic/gin"
	"bitbucket.org/taodongl/gone/middleware/jwt"
)

func main() {
	// gin.SetMode(gin.ReleaseMode)
	r := gin.Default()
	jwt.PrintJWT()
	r.Run(":8080")
}
